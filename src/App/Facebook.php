<?php

namespace App\Library;

use App\Library;
use Facebook\Facebook as Facebook;

class FacebookApi {

    /**
     * Constructor
     */
    public function __construct(){

	    $ini = parse_ini_file(__DIR__ . '/../../config/facebook.ini');

	    $app_id                = $ini['app_id'];
	    $app_secret            = $ini['app_secret'];
	    $default_graph_version = $ini['default_graph_version'];

	    $fb = new Facebook([
            'app_id'     => $app_id,
            'app_secret' => $app_secret,
            'default_graph_version' => $default_graph_version
        ]);

	    return $fb;
    }

}