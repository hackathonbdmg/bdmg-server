<?php

namespace App\Service;

use App\Service;
use PDO;
use App\Library\GeneratorImage as GeneratorImage;

class User extends Service
{

    public $log_file = "user_service.log";

    /**
     * Recupera o usuário pelo token
     */
    public function getUserByToken($token){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT
                tb1.id_user
            FROM 
                tbl_user tb1
            WHERE
                tb1.token = :token
        ");
        $query->bindParam(':token', $token);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return $line;
        } else {
            return array();
        }
    }

    /**
     * Get user profile
     */
    public function getUserProfile($id_user){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                 tb1.id_user
                ,tb1.email
                ,tb1.old_password
                ,tb1.token
                ,tb1.confirmed_email
                ,tb1.name
                ,tb1.status_active
                ,tb1.cpf
                ,tb1.phone
                ,tb1.dt_birth
                ,tb1.dt_last_login
                ,tb1.dt_create
                ,tb1.device_token
                ,tb1.platform
            FROM 
                tbl_user tb1
            WHERE
                tb1.id_user = :id_user
        ");
        $query->bindParam(':id_user', $id_user);

        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            $line['dt_create']     = strtotime($line['dt_create']);
            $line['dt_last_login'] = strtotime($line['dt_last_login']);
            return $line;
        } else {
            return array();
        }
    }

    /**
     * Get user password
     */
    public function getUserPassword($id_user){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                 tb1.password
                ,tb1.old_password
            FROM 
                tbl_user tb1
            WHERE
                tb1.id_user = :id_user
        ");
        $query->bindParam(':id_user', $id_user);

        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return $line;
        } else {
            return array();
        }
    }

    /**
     * Register User
     * @param $id_user
     * @param $name
     * @param $phone
     * @return array
     */
    public function registerUser($params){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            INSERT INTO tbl_user 
                (email, password, old_password) 
            VALUES 
                (:email, :password, :old_password)
        ");

        $password = !empty($params['password']) ? md5($params['password']) : null;

        $query->bindValue(':email', !empty($params['email']) ? $params['email'] : null);
        $query->bindParam(':password', $password);
        $query->bindParam(':old_password', $password);
        $query->execute();

        $status  = $this->verifyBadExecute($query);
        $id_user = $conn->lastInsertId('id_user_seq');
        $conn    = null;

        if($status){
            return $id_user;
        } else{
            return $status;
        }
    }

    /**
     * Verify exist email
     * @param $email
     * @return array
     */
    public function verifyExistEmail($email){
        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                id_user
            FROM 
                tbl_user
            WHERE 
                email = :email
        ");

        $query->bindParam(':email', $email);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update User
     * @param $id_user
     * @return array
     */
    public function updateUser($id_user, $params){

        $conn = $this->getConnection();

        $query = $conn->prepare('
            UPDATE 
                tbl_user
            SET 
                 email         = :email
                ,name          = :name
                ,dt_birth      = :dt_birth
                ,cpf           = :cpf
                ,phone         = :phone
                ,password      = :password
                ,old_password  = :old_password
            WHERE 
                id_user = :id_user
        ');

        $user_password = $this->getUserPassword($id_user);

        $password     = !empty($params['password']) ? md5($params['password']) : $user_password['password'];
        $old_password = !empty($params['password']) ? md5($params['password']) : $user_password['old_password'];

        $query->bindParam(':id_user', $id_user);
        $query->bindValue(':email', !empty($params['email']) ? $params['email'] : null);
        $query->bindValue(':name', !empty($params['name']) ? $params['name'] : null);
        $query->bindValue(':dt_birth', !empty($params['dt_birth']) ? $params['dt_birth'] : null);
        $query->bindValue(':cpf', $params['cpf']);
        $query->bindValue(':phone', !empty($params['phone']) ? $params['phone'] : null);
        $query->bindValue(':password', $password);
        $query->bindValue(':old_password', $old_password);
        $query->execute();

        $status  = $this->verifyBadExecute($query);
        $conn    = null;

        return $status;
    }

    /**
     * Update User
     * @param $id_user
     * @param $email
     * @param $name
     * @param $phone
     * @return array
     */
    public function bupdateUser($id_user, $params){

        $conn = $this->getConnection();

        $user = $this->getUserProfile($id_user);
        if(!$user){
            return false;
        }

        $query = $conn->prepare('
            UPDATE 
                tbl_user
            SET 
                 email         = :email
                ,name          = :name
                ,dt_birth      = :dt_birth
                ,cpf           = :cpf
                ,phone         = :phone
                ,password      = :password
                ,old_password  = :old_password
            WHERE 
                id_user = :id_user
        ');

        $password     = !empty($params['password']) ? md5($params['password']) : $user['password'];
        $old_password = !empty($params['password']) ? md5($params['password']) : $user['old_password'];
        $email        = !empty($params['email']) ? $params['email'] : $user['email'];
        $name         = !empty($params['name']) ? $params['name']  : $user['name'];
        $cpf          = !empty($params['cpf']) ? $params['cpf']  : $user['cpf'];
        $dt_birth     = !empty($params['dt_birth']) ? $params['dt_birth']  : $user['dt_birth'];
        $phone        = !empty($params['phone']) ? $params['phone']  : $user['phone'];

        $query->bindValue(':id_user', $id_user);
        $query->bindValue(':email', $email);
        $query->bindValue(':name', $name);
        $query->bindValue(':dt_birth', $dt_birth);
        $query->bindValue(':cpf', $cpf);
        $query->bindValue(':phone', $phone);
        $query->bindValue(':password', $password);
        $query->bindValue(':old_password', $old_password);
        $query->execute();

        $status  = $this->verifyBadExecute($query);
        $conn    = null;

        return $status;
    }

    /**
     * Update field
     * @param $id_user
     * @param $field
     * @param $value
     * @return array
     */
    public function Patch($id, $field, $value){

        $conn = $this->getConnection();

        if($field == 'password'){
            $value = md5($value);
            $query = $conn->prepare('UPDATE tbl_user set password = :x, old_password = :x where id_user = :id');
        } else{
            $query = $conn->prepare('UPDATE tbl_user set '.$field.' = :x where id_user = :id');
        }

        $query->execute(array(
            ':id' => $id,
            ':x'  => $value
        ));

        $status = $this->verifyBadExecute($query);
        $conn = null;

        return $status;
    }

    /**
     * Atualiza a senha do usuário
     * @param $email
     * @return array
     */
    public function forgotPassword($email){

        $conn = $this->getConnection();

        $new_password  = $this->generateRandomPassword();
        $hash_password = md5($new_password);

        $query = $conn->prepare('UPDATE tbl_user set password = :x where email = :email');
        $query->execute(array(
            ':email' => $email,
            ':x'     => $hash_password
        ));

        $status = $this->verifyBadExecute($query);
        $conn = null;

        if($status){
            return $new_password;
        } else{
            return false;
        }
    }

    /**
     * Get Already Exist email
     * @param $email
     * @return array
     */
    public function getAlreadyExistEmail($email, $id_user = false){

        $conn = $this->getConnection();

        $query = "
            SELECT 
                id_user
            FROM 
                tbl_user
            WHERE
                email = :email
        ";

        if($id_user){
            $query .= " AND id_user != :id_user ";
        }

        $query = $conn->prepare($query);

        if($id_user){
            $query->bindParam(':id_user', $id_user);
        }

        $query->bindParam(':email', $email);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * delete user
     * @param $id_user
     * @return boolean
     */
    public function delete($id_user){

        $conn  = $this->getConnection();
        $query = $conn->prepare("DELETE FROM tbl_user WHERE id_user = :user");

        $query->bindParam(':user', $id_user);
        $query->execute();
        $status = $this->verifyBadExecute($query);
        $conn   = null;
        return $status;
    }

    /**
     * Get user by email
     * @return array
     */
    public function getUserByEmail($email){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                email, confirmed_email, dt_last_login, dt_create, status_active, name
            FROM 
                tbl_user tb1
            WHERE
                tb1.email = :email
        ");

        $query->bindParam(':email', $email);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            $line['dt_create']  = strtotime($line['dt_create']);
            $line['dt_last_login'] = strtotime($line['dt_last_login']);
            return $line;
        } else {
            return false;
        }
    }

    /**
     * Confirma email do usuario
     */
    public function confirmEmail($user, $email){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            UPDATE
                tbl_user 
            SET 
                confirmed_email = true
            WHERE 
                id_user = :user
                AND
                email = :email
        ");
        $query->bindParam(':user', $user);
        $query->bindParam(':email', $email);
        $query->execute();
        $status = $this->verifyBadExecute($query);
        $conn   = null;
        return $status;
    }

    /**
     * Update last login
     * @return array
     */
    public function updateLastLogin($user_id){

        $conn = $this->getConnection();

        $query = $conn->prepare('UPDATE tbl_user SET dt_last_login = current_timestamp WHERE id_user = :id_user');
        $query->bindValue(':id_user', $user_id);
        $query->execute();

        $status  = $this->verifyBadExecute($query);
        $conn    = null;

        return $status;
    }

    /**
     * Update status active
     * @return array
     */
    public function updateStatusActive($id_user, $params){

        $conn = $this->getConnection();

        if($params['status']){
            $status = 'true';
        } else{
            $status = 'false';
        }

        $query = $conn->prepare('UPDATE tbl_user SET status_active = :status_active WHERE id_user = :id_user');
        $query->bindValue(':status_active', $status);
        $query->bindValue(':id_user', $id_user);
        $query->execute();

        $status  = $this->verifyBadExecute($query);
        $conn    = null;

        return $status;
    }

    /**
     * Atualiza as informações do dispositivo do usuário
     */
    public function updateDeviceInfo($id_user, $params){
        $conn = $this->getConnection();

        $query = $conn->prepare("
            UPDATE 
                tbl_user
            SET 
                 platform     = :platform
                ,device_token = :device_token
            WHERE 
                id_user = :id_user
        ");

        $query->bindParam(':platform', $params['platform']);
        $query->bindParam(':device_token', $params['device_token']);
        $query->bindParam(':id_user', $id_user);

        $query->execute();
        $status = $this->verifyBadExecute($conn);
        $conn = null;

        return $status;
    }

    /**
     * Update user password
     */
    public function updateUserPassword($id_user, $password){
        $conn = $this->getConnection();

        $query = $conn->prepare("
            UPDATE 
                tbl_user
            SET 
                password = :password
            WHERE 
                id_user = :id_user
        ");

        $query->bindValue(':password', md5($password));
        $query->bindParam(':id_user', $id_user);

        $query->execute();
        $status = $this->verifyBadExecute($conn);
        $conn = null;

        return $status;
    }

}