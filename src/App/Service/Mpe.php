<?php

namespace App\Service;

use App\Service;
use PDO;
use App\Library\GeneratorImage as GeneratorImage;

class Mpe extends Service
{

    public $log_file = "mpe_service.log";

    /**
     * Register Mpe
     * @param $id_mpe
     * @param $name
     * @param $phone
     * @return array
     */
    public function registerMpe($id_user, $params){

        $conn = $this->getConnection();

        $query = $conn->prepare("
			INSERT INTO mpe
				(id_user, razao_social, cnpj, county, year_billing, simples, address)
			VALUES
				(:id_user, :razao_social, :cnpj, :county, :year_billing, :simples, :address)
        ");

        $password = !empty($params['password']) ? md5($params['password']) : null;

        $query->bindValue(':id_user', $id_user);
        $query->bindParam(':razao_social', $params['razao_social']);
        $query->bindParam(':cnpj', $params['cnpj']);
        $query->bindValue(':year_billing', $params['year_billing']);
        $query->bindParam(':county', $params['county']);
        $query->bindParam(':simples', $params['simples']);
        $query->bindParam(':address', $params['address']);
        $query->execute();

        $status  = $this->verifyBadExecute($query);
        $id_mpe = $conn->lastInsertId('id_mpe_seq');
        $conn    = null;

        if($status){
            return $id_mpe;
        } else{
            return $status;
        }
    }

}