<?php

namespace App\Service;

use App\Service;
use PDO;

class Project extends Service
{

    public $log_file = "project_service.log";

    /**
     * Register Project
     * @param $id_project
     * @param $name
     * @param $phone
     * @return array
     */
    public function registerProject($id_mpe, $params){

        $conn = $this->getConnection();


        $query = $conn->prepare("
			INSERT INTO project
				(id_mpe, month_consumption, financing_time_month, financing_tax, plot_value, project_value, initial_payment)
			VALUES
				(:id_mpe, :month_consumption, :financing_time_month, :financing_tax, :plot_value, :project_value, :initial_payment)
        ");

        $query->bindValue(':id_mpe', $id_mpe);
        $query->bindValue(':month_consumption', $params['month_consumption']);
        $query->bindParam(':financing_time_month', $params['financing_time_month']);
        $query->bindParam(':financing_tax', $params['financing_tax']);
        $query->bindValue(':plot_value', $params['plot_value']);
        $query->bindParam(':project_value', $params['project_value']);
        $query->bindParam(':initial_payment', $params['initial_payment']);
        $query->execute();

        $status  = $this->verifyBadExecute($query);
        $id_project = $conn->lastInsertId('id_project_seq');
        $conn    = null;

        if($status){
            return $id_project;
        } else{
            return $status;
        }
    }

}