<?php

namespace App\Service;

use App\Service;
use PDO;

class Contact extends Service
{

    public $log_file = "contact_service.log";

    /**
     * Get count all contacts
     * @return array
     */
    public function getCountContacts($params){

        $conn = $this->getConnection();
        $query = "
			SELECT
				count(*) as total
			FROM
				contact tb1
			WHERE
				1=1
        ";

        if(!empty($params['name'])){
            $condition = $this->makeConditionSplitText('name', 'tb1', 'contact', $params['name']);
            $query .= " AND ".$condition;
        }

        if(!empty($params['dt_contact'])){
            $query .= " AND (tb1.dt_create AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = :dt_contact ";
        }

        if(!empty($params['id_contact_origin'])){
            $query .= " AND tb1.id_contact_origin = :id_contact_origin ";
        }

        $query = $conn->prepare($query);

        if(!empty($params['dt_contact'])){
            $query->bindParam(':dt_contact', $params['dt_contact']);
        }

        if(!empty($params['id_contact_origin'])){
            $query->bindParam(':id_contact_origin', $params['id_contact_origin']);
        }

        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if($line['total'] > 0){
            return $line['total'];
        } else {
            return 0;
        }
    }

    /**
     * Get all contacts
     * @return array
     */
    public function getContacts($params){

        $conn = $this->getConnection();
        $query = "
			SELECT
				 tb1.id_contact
				,tb1.email
				,tb1.name
				,tb1.message
				,tb1.phone
                ,tb1.dt_create
				,tb1.subject
			FROM
				contact tb1
			WHERE
				1=1
        ";

        if(!empty($params['name'])){
            $condition = $this->makeConditionSplitText('name', 'tb1', 'contact', $params['name']);
            $query .= " AND ".$condition;
        }

        if(!empty($params['dt_contact'])){
            $query .= " AND (tb1.dt_create AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = :dt_contact ";
        }

        if(!empty($params['id_contact_origin'])){
            $query .= " AND tb1.id_contact_origin = :id_contact_origin ";
        }

        $query .= "
            ORDER BY
                tb1.id_contact DESC
            LIMIT
                :limit
            OFFSET
                :offset
        ";

        $query = $conn->prepare($query);

        if(!empty($params['dt_contact'])){
            $query->bindParam(':dt_contact', $params['dt_contact']);
        }

        if(!empty($params['id_contact_origin'])){
            $query->bindParam(':id_contact_origin', $params['id_contact_origin']);
        }

        $query->bindValue(':limit', !empty($params['limit']) ? $params['limit'] : 99999);
        $query->bindValue(':offset', !empty($params['offset']) ? $params['offset'] : 0);

        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($data)){
            return $data;
        } else {
            return array();
        }
    }

    /**
     * Register contact
     * @return array
     */
    public function registerContact($params){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            INSERT INTO contact
                (id_contact_origin, email, name, message, phone, subject)
            VALUES
                (:id_contact_origin, :email, :name, :message, :phone, :subject)
        ");

        $query->bindParam(':id_contact_origin', $params['id_contact_origin']);
        $query->bindParam(':email', $params['email']);
        $query->bindParam(':name', $params['name']);
        $query->bindParam(':message', $params['message']);
        $query->bindParam(':phone', $params['phone']);
        $query->bindValue(':subject', !empty($params['subject']) ? $params['subject'] : null);
        $query->execute();

        $status = $this->verifyBadExecute($query);
        $id     = $conn->lastInsertId('id_contact_seq');
        $conn   = null;

        if($status){
            return $id;
        } else{
            return $status;
        }
    }
}

?>