<?php

namespace App\Service;

use App\Service;
use \Gamegos\JWS\JWS as Gamegos;
use PDO;

class Authorize extends Service
{
    public $log_file = "authorize_service.log";

    /**
     * @param $email, $password
     * @return object
     */
    public function getUserId($email, $password){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                tb1.id_user
            FROM 
                tbl_user tb1
            WHERE 
                tb1.email = :email
                AND
                (
                    tb1.password = :password
                    OR
                    tb1.old_password = :password
                )
                AND
                tb1.status_active IS TRUE

        ");
        
        $query->bindParam(':email', $email, PDO::PARAM_STR);
        $query->bindValue(':password', md5($password), PDO::PARAM_STR);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return $line['id_user'];
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return object
     */
    public function verifyExistingToken($user_id){
        
        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                token, token_expire
            FROM 
                tbl_user
            WHERE 
                token_expire > localtimestamp
            AND
                id_user = :user_id
        ");

        $query->bindParam(':user_id', $user_id);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return array(
                'access_token' => $line['token'],
                'expires'      => strtotime($line['token_expire'])
            );
        } else {
            return false;
        }
    }

    /**
     * @param $email
     * @param $password
     * @param $user_id
     * @return array
     */
    public function createToken($email, $password, $id_user){

        $conn             = $this->getConnection();
        $token            = $this->makeToken($email, $password, $id_user);
        $expire_timestemp = time() + (120 * 60) * 5000;
        $token_expire     = date('D, d M Y H:i:s T', $expire_timestemp);

        $query = $conn->prepare("
            UPDATE 
                tbl_user
            SET
                token = :token,
                token_expire = :token_expire
            WHERE
                id_user = :id
        ");

        $query->bindParam(':token', $token);
        $query->bindParam(':token_expire', $token_expire);
        $query->bindParam(':id', $id_user);
        $query->execute();

        $status = $this->verifyBadExecute($conn);
        $conn = null;
        if($status){
            return array(
                'access_token' => $token,
                'expires'      => $expire_timestemp
            );
        } else{
            return false;
        }
    }

    /**
     * @param $email
     * @param $password
     * @param $user_id
     * @return int
     */
    public function makeToken($email, $password, $user){

        $headers = array(
            'alg' => 'HS256',
            'typ' => 'JWT'
        );

        $payload = array(
            'email'     => $email,
            'password'  => $password,
            'user_id'   => $user,
            'timestamp' => time()
        );

        $key = 'UGx1cml0ZWNoIEJyYXNpbCBMdGRhDQpMdWlzDQpHdWlsaGVybWUNClBlZHJv';

        $jws = new Gamegos();
        return $jws->encode($headers, $payload, $key);
    }

    /**
     * @param $id
     * @return object
     */
    public function verifyTokenAdmin($token){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                tb1.email, tb2.id_admin, tb1.id_user, tb2.super
            FROM tbl_user tb1
                inner join admin tb2 on (tb2.id_user = tb1.id_user)
            WHERE 
                tb1.token_expire > localtimestamp
            AND
                tb1.token = :token
        ");

        $query->bindParam(':token', $token);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return $line;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return object
     */
    public function verifyTokenUser($token){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                 tb1.id_user
                ,tb2.id_supplier
                ,tb3.id_customer
                ,tb4.id_admin
                ,tb1.email
                ,tb1.status_active
                ,tb1.name
                ,tb1.phone
            FROM tbl_user tb1 
                left join supplier tb2 on (tb2.id_user = tb1.id_user)
                left join customer tb3 on (tb3.id_user = tb1.id_user)
                left join admin    tb4 on (tb4.id_user = tb1.id_user)
            WHERE 
                tb1.token_expire > localtimestamp
            AND
                tb1.token = :token
        ");

        $query->bindParam(':token', $token);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return $line;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return object
     */
    public function verifyTokenCustomer($token){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                 tb1.id_user
                ,tb1.email
                ,tb1.status_active
                ,tb1.name
                ,tb2.id_customer
                ,tb1.phone
                ,tb2.id_user_face
                ,tb2.id_gateway
            FROM tbl_user tb1 
                inner join customer tb2 on (tb2.id_user = tb1.id_user)
            WHERE 
                tb1.token_expire > localtimestamp
            AND
                tb1.token = :token
        ");

        $query->bindParam(':token', $token);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return $line;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return object
     */
    public function verifyTokenSupplier($token){

        $conn = $this->getConnection();

        $query = $conn->prepare("
            SELECT 
                 tb1.id_user
                ,tb1.email
                ,tb1.status_active
                ,tb1.name
                ,tb2.id_supplier
                ,tb1.phone
            FROM tbl_user tb1 
                inner join supplier tb2 on (tb2.id_user = tb1.id_user)
            WHERE 
                tb1.token_expire > localtimestamp
            AND
                tb1.token = :token
        ");

        $query->bindParam(':token', $token);
        $query->execute();
        $line = $query->fetch(PDO::FETCH_ASSOC);
        $conn = null;

        if(!empty($line)){
            return $line;
        } else {
            return false;
        }
    }
}