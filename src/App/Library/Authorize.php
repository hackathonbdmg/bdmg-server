<?php

namespace App\Library;

use App\Library;
use App\Service\Authorize as AuthorizeService;
use App\Service\User as UserService;

class Authorize extends Library
{
    /**
     * @var \App\Service\Auth
     */
    private $authorizeService;

    /**
     * Get auth service
     */
    public function init(){

       $this->setAuthorizeService(new AuthorizeService());
    }

    /**
     * @return bool
     */
    public function verifyTokenAdmin($token = null, $stop = true){

        if(!empty($token)){
            $admin = $this->getAuthorizeService()->verifyTokenAdmin($token);
            if($admin){
                $env = $this->getSlim()->environment();
                $env['admin']        = $admin;
                $env['kind_of_user'] = 'admin';
            } else {
                if($stop){
                    self::response(self::STATUS_BAD_REQUEST, array(
                        'status' =>  "Unauthorized",
                        'message' => "Invalid token."
                        )
                    );
                    $this->getSlim()->stop();
                }
            }
        } else {
            if($stop){
                self::response(self::STATUS_BAD_REQUEST, array(
                    'status' =>  "BadRequest",
                    'message' => "Missing token"
                    )
                );
                $this->getSlim()->stop();
            }
        }
    }

    /**
     * @return bool
     */
    public function verifyTokenUser($token = null, $stop = true){

        if(!empty($token)){
            $user = $this->getAuthorizeService()->verifyTokenUser($token);
            if($user){

                $env = $this->getSlim()->environment();
                $env['user'] = $user;

                if(!empty($user['id_customer'])){
                    $env['kind_of_user'] = 'customer';
                    $env['customer']     = $this->getAuthorizeService()->verifyTokenCustomer($token);
                }

                if(!empty($user['id_supplier'])){
                    $env['kind_of_user'] = 'supplier';
                    $env['supplier']     = $this->getAuthorizeService()->verifyTokenSupplier($token);
                }

                if(!empty($user['id_admin'])){
                    $env['kind_of_user'] = 'admin';
                    $env['admin']        = $this->getAuthorizeService()->verifyTokenAdmin($token);
                }
            } else {
                if($stop){
                    self::response(self::STATUS_BAD_REQUEST, array(
                        'status' =>  "Unauthorized",
                        'message' => "Invalid token."
                        )
                    );
                    $this->getSlim()->stop();
                }
            }
        } else {
            if($stop){
                self::response(self::STATUS_BAD_REQUEST, array(
                    'status' =>  "BadRequest",
                    'message' => "Missing token"
                    )
                );
                $this->getSlim()->stop();
            }
        }
    }

    /**
     * @return bool
     */
    public function verifyTokenCustomer($token = null, $stop = true){

        if(!empty($token)){
            $customer = $this->getAuthorizeService()->verifyTokenCustomer($token);
            if($customer){
                $env = $this->getSlim()->environment();
                $env['customer']     = $customer;
                $env['kind_of_user'] = 'customer';
            } else {
                if($stop){
                    self::response(self::STATUS_BAD_REQUEST, array(
                        'status' =>  "Unauthorized",
                        'message' => "Invalid token."
                        )
                    );
                    $this->getSlim()->stop();
                }
            }
        } else {
            if($stop){
                self::response(self::STATUS_BAD_REQUEST, array(
                    'status' =>  "BadRequest",
                    'message' => "Missing token"
                    )
                );
                $this->getSlim()->stop();
            }
        }
    }

    /**
     * @return bool
     */
    public function verifyTokenSupplier($token = null, $stop = true){

        if(!empty($token)){
            $supplier = $this->getAuthorizeService()->verifyTokenSupplier($token);
            if($supplier){
                $env = $this->getSlim()->environment();
                $env['supplier'] = $supplier;
                $env['kind_of_user'] = 'supplier';
            } else {
                if($stop){
                    self::response(self::STATUS_BAD_REQUEST, array(
                        'status' =>  "Unauthorized",
                        'message' => "Invalid token."
                        )
                    );
                    $this->getSlim()->stop();
                }
            }
        } else {
            if($stop){
                self::response(self::STATUS_BAD_REQUEST, array(
                    'status' =>  "BadRequest",
                    'message' => "Missing token"
                    )
                );
                $this->getSlim()->stop();
            }
        }
    }

    /**
     * @return \App\Service\User
     */
    public function getAuthorizeService(){

        return $this->authorizeService;
    }

    /**
     * @param \App\Service\User $userService
     */
    public function setAuthorizeService($authorizeService){

        $this->authorizeService = $authorizeService;
    }
}