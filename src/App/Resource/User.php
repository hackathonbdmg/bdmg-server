<?php

namespace App\Resource;

use App\Resource;
use App\Service\User as UserService;
use App\Service\Biker as BikerService;
use App\Service\Client as ClientService;

class User extends Resource
{
    /**
     * @var \App\Service\User
     */
    private $userService;
    private $log_file = "user_resource.log";

    /**
     * Get user service
     */
    public function init(){

        $this->setUserService(new UserService());
    }

    /**
     * Create user
     */
    public function registerUser($obj_params){

        $this->checkParamsCreate($obj_params);

        $this->checkAlreadyExistEmail($obj_params);

        $user = $this->getUserService()->registerUser($obj_params);
        if(!$user){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status'   => "InternalServerError",
                'message'  => "Internal server error on register user."
                )
            );
            $this->getSlim()->stop();
        }

        return $user;
    }

    /**
     * Update user
     */
    public function updateUser($id_user, &$params){

        $params = $this->getParamsToUpdate($id_user, $params);

        if(!empty($params['email'])){
            if($this->getUserService()->getAlreadyExistEmail($params['email'], $id_user)){
                self::response(self::STATUS_BAD_REQUEST, array(
                    'status'   => "EmailAlreadyExist",
                    'message'  => "Email Already Exist."
                    )
                );
                $this->getSlim()->stop();
            }
        }

        $status = $this->getUserService()->updateUser($id_user, $params);
        if(!$status){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status'   => "InternalServerError",
                'message'  => "Internal server error on update user."
                )
            );
            $this->getSlim()->stop();
        }

        return $status;
    }

    /**
     * Get params to update
     */
    public function getParamsToUpdate($id_user, &$params){

        $user = $this->getUserService()->getUserProfile($id_user);
        if(!$user){
            return false;
        }

        $params['email']    = !empty($params['email'])    ? $params['email'] : $user['email'];
        $params['name']     = !empty($params['name'])     ? $params['name']  : $user['name'];
        $params['cpf']      = !empty($params['cpf'])      ? $params['cpf']  : $user['cpf'];
        $params['dt_birth'] = !empty($params['dt_birth']) ? $params['dt_birth']  : $user['dt_birth'];
        $params['phone']    = !empty($params['phone'])    ? $params['phone']  : $user['phone'];

        if(!empty($params['cnpj'])){
            $params['cpf'] = null;
        }
        
        return $params;

    }

    /**
     * checkAlreadyExistEmail
     */
    public function checkAlreadyExistEmail($obj_params){

        if($this->getUserService()->getAlreadyExistEmail($obj_params['email'])){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "EmailAlreadyExist",
                'message'  => "Email Already Exist."
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Validade create params
     */
    public function checkParamsCreate($obj_params){

        if (
            empty($obj_params['email'])
        ){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Missing fields",
                'required' => array('email')
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Delete user
     */
    public function delete($id){
        $status = $this->getUserService()->delete($id);
        if(!$status){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status'   => "InternalServerError",
                'message'  => "Internal server error on delete user."
                )
            );
            $this->getSlim()->stop();
        }

        return $status;
    }

    /**
     * Confirma o email de um determinado usuário
     */
    public function confirmEmail($user, $email){

        //Decodifica
        $user  = base64_decode($user);
        $email = base64_decode($email);

        //Valida entrada correta
        if($user && $email){

            //Confirma email do usuário
            $status = $this->getUserService()->confirmEmail($user, $email);
            if(!$status){
                self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                    'status' =>  "InternalServerError",
                    'message' => "Internal server error on confirm you email. "
                    )
                );
                $this->getSlim()->stop();
            }
        } else{
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status' =>  "BadRequest",
                'message' => "Missing fields. "
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Update status active
     */
    public function updateStatusActive($id_user){
        
        $obj_params = $this->getBodyRequest();

        if(!isset($obj_params['status'])){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'  => "BadRequest",
                'message' => "Missing fields"
                )
            );
            $this->getSlim()->stop();
        }

        $status = $this->getUserService()->updateStatusActive($id_user, $obj_params);
        if(!$status){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status' =>  "InternalServerError",
                'message' => "Internal server error on update status active. "
                )
            );
            $this->getSlim()->stop();
        }

        self::response(self::STATUS_NO_CONTENT, array());
    }

    /**
     * Atualiza as informações do dispositivo do usuário
     */
    public function updateDeviceInfo($id_user){
        
        $env        = $this->getSlim()->environment('user');
        $obj_params = $this->getBodyRequest();

        if(empty($obj_params['device_token']) || empty($obj_params['platform'])){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Missing fields",
                'required' => array('device_token', 'platform')
                )
            );
            $this->getSlim()->stop();
        }

        if(
            ($env['kind_of_user'] == 'supplier' && $env['supplier']['id_user'] != $id_user)
            ||
            ($env['kind_of_user'] == 'customer' && $env['customer']['id_user'] != $id_user)
        ){
            self::response(self::STATUS_UNAUTHORIZED, array(
                'status' =>  "Unauthorized",
                'message' => "You are not authorized. "
                )
            );
            $this->getSlim()->stop();  
        }

        $status = $this->getUserService()->updateDeviceInfo($id_user, $obj_params);
        if(!$status){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status' =>  "InternalServerError",
                'message' => "Internal server on update user device info. "
                )
            );
            $this->getSlim()->stop();
        }

        self::response(self::STATUS_NO_CONTENT);
    }

    /**
     * Show options in header
     */
    public function options(){

        self::response(self::STATUS_OK, array(), array('POST', 'OPTIONS'));
    }

    /**
     * @return \App\Service\User
     */
    public function getUserService(){

        return $this->userService;
    }

    /**
     * @param \App\Service\AUthorize $userService
     */
    public function setUserService($userService){

        $this->userService = $userService;
    }

    /**
     * @return array
     */
    public function getOptions(){

        return $this->options;
    }
}