<?php

namespace App\Resource;

use App\Resource;
use App\Service\Mpe as MpeService;
use App\Resource\User as UserResource;
use App\Resource\Project as ProjectResource;

class Mpe extends Resource
{
    /**
     * @var \App\Service\Mpe
     */
    private $mpeService;
    private $log_file = "mpe_resource.log";

    /**
     * Get mpe service
     */
    public function init(){

        $this->setMpeService(new MpeService());
    }

    /**
     * Register mpe
     */
    public function registerMpe(){

        $userResource    = new UserResource();
        $projectResource = new ProjectResource();

        $obj_params = $this->getBodyRequest();
        $userResource->checkParamsCreate($obj_params);
        $this->checkParamsCreate($obj_params);

        $id_user = $userResource->registerUser($obj_params);

        $id_mpe = $this->getMpeService()->registerMpe($id_user, $obj_params);
        if(!$id_mpe){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status'   => "InternalServerError",
                'message'  => "Internal server error on register mpe."
                )
            );
            $this->getSlim()->stop();
        }

        $id_project = $projectResource->registerProject($id_mpe, $obj_params);

        self::response(self::STATUS_OK, array('id' => $id_mpe, 'id_project' => $id_project));
    }


    /**
     * Validade create params
     */
    public function checkParamsCreate($obj_params){

        if (
               empty($obj_params['razao_social'])
            || empty($obj_params['cnpj'])
            || empty($obj_params['county'])
            || empty($obj_params['year_billing'])
            || !isset($obj_params['simples'])
            || empty($obj_params['address'])
        ){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Missing fields",
                'required' => array(
                     'razao_social'
                    ,'cnpj'
                    ,'county'
                    ,'year_billing'
                    ,'simples'
                    ,'address'
                )
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Show options in header
     */
    public function options(){

        self::response(self::STATUS_OK, array(), array('POST', 'OPTIONS'));
    }

    /**
     * @return \App\Service\Mpe
     */
    public function getMpeService(){

        return $this->mpeService;
    }

    /**
     * @param \App\Service\AUthorize $mpeService
     */
    public function setMpeService($mpeService){

        $this->mpeService = $mpeService;
    }

    /**
     * @return array
     */
    public function getOptions(){

        return $this->options;
    }
}