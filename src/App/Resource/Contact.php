<?php

namespace App\Resource;

use App\Resource;
use App\Service\Contact as ContactService;
use App\Service\ContactOrigin as ContactOriginService;
use App\Library\Csv as Csv;

class Contact extends Resource
{
    /**
     * @var \App\Service\Contact
     */
    private $contactService;
    private $log_file = "contact_resource.log";

    /**
     * Get contact service
     */
    public function init(){

        $this->setContactService(new ContactService());
    }

    /**
     * get contacts
     */
    public function getContacts(){

        $env    = $this->getSlim()->environment('user');
        $req    = $this->getSlim()->request();
        $params = array(
            'name'              => $req->get('name'),
            'dt_contact'        => $req->get('dt-contact'),
            'id_contact_origin' => $req->get('id-contact-origin'),
            'limit'             => $req->get('limit'),
            'offset'            => $req->get('offset')
        );

        $contact['total']   = $this->getContactService()->getCountContacts($params);
        $contact['contact'] = $this->getContactService()->getContacts($params);

        self::response(self::STATUS_OK, $contact);
    }

    /**
     * Register contact
     */
    public function registerContact(){

        $env                  = $this->getSlim()->environment('user');
        $contactOriginService = new ContactOriginService();

        $obj_params = $this->getBodyRequest();
        $this->checkParamsCreate($obj_params);

        $obj_params['phone'] = empty($obj_params['phone']) ? $env['user']['phone'] : null;

        $contact_origin = $contactOriginService->getContactOriginByCode($obj_params['origin_code']);
        if(!$contact_origin){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Invalid origin code."
                )
            );
            $this->getSlim()->stop();
        } else{
            $obj_params['id_contact_origin'] = $contact_origin['id_contact_origin'];
        }

        $id_contact = $this->getContactService()->registerContact($obj_params);
        if(!$id_contact){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status'   => "InternalServerError",
                'message'  => "Internal server error on register contact."
                )
            );
            $this->getSlim()->stop();
        }

        self::response(self::STATUS_CREATED, array('id_contact' => $id_contact));
    }

    /**
     * Validade create params
     */
    public function checkParamsCreate($obj_params){

        if (
               empty($obj_params['subject'])
            || empty($obj_params['email'])
            || empty($obj_params['name'])
            || empty($obj_params['message'])
            || empty($obj_params['origin_code'])
        ){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Missing fields",
                'required' => array(
                     'subject'
                    ,'email'
                    ,'name'
                    ,'message'
                    ,'origin_code'
                )
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Show options in header
     */
    public function options(){

        self::response(self::STATUS_OK, array(), array('POST', 'OPTIONS'));
    }

    /**
     * @return \App\Service\Contact
     */
    public function getContactService(){

        return $this->contactService;
    }

    /**
     * @param \App\Service\AUthorize $contactService
     */
    public function setContactService($contactService){

        $this->contactService = $contactService;
    }

    /**
     * @return array
     */
    public function getOptions(){

        return $this->options;
    }
}