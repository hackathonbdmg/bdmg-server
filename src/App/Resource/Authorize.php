<?php

namespace App\Resource;

use App\Resource;
use App\Service\Authorize as AuthorizeService;
use App\Service\Supplier as SupplierService;
use App\Service\Customer as CustomerService;
use App\Service\User as UserService;
use App\Service\Admin as AdminService;
use App\Library\Email as MyEmail;
use Exception;
use \DOMDocument;

class Authorize extends Resource
{
    /**
     * @var \App\Service\Auth
     */
    private $authorizeService;
    private $log_file = "authorize.log";

    /**
     * Get auth service
     */
    public function init(){

        $this->setAuthorizeService(new AuthorizeService());
    }

    /**
     * Create auth
     */
    public function post(){

        //$bikerService       = new BikerService();
        //$clientService      = new ClientService();

        $obj_params = $this->getBodyRequest();
        $this->checkParamsCreate($obj_params);

        $id_user = $this->getAuthorizeService()->getUserId($obj_params['email'], $obj_params['password']);
        if(!$id_user){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status' =>  "UserNotExists",
                'message' => "Invalid user or inactivated."
                )
            );
            $this->getSlim()->stop();
        }

        $answer = $this->getToken($id_user, $obj_params['email'], $obj_params['password']);

        self::response(self::STATUS_CREATED, $answer);
    }

    /**
     * Verify token
     */
    public function getToken($id_user, $email, $password){

        $userService     = new UserService();
        $adminService    = new AdminService();
        $supplierService = new SupplierService();
        $customerService = new CustomerService();

        $old_token = $this->getAuthorizeService()->verifyExistingToken($id_user);
        if(!$old_token){
            $new_token = $this->getAuthorizeService()->createToken($email, $password, $id_user);
            if(!$new_token){
                self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                    'status' =>  "InternalServerError",
                    'message' => "Internal server error. On try create a new token for the user. "
                    )
                );
                $this->getSlim()->stop();
            }
        }

        $status = $userService->updateLastLogin($id_user);
        if(!$status){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status' =>  "InternalServerError",
                'message' => "Internal server error on try update last login date. "
                )
            );
            $this->getSlim()->stop();
        }

        $answer            = $old_token ? $old_token : $new_token;
        $answer['id_user'] = $id_user;

        $user = $this->getAuthorizeService()->verifyTokenUser($answer['access_token']);
        if(!$user){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status' =>  "InternalServerError",
                'message' => "Internal server error on verify token user."
                )
            );
            $this->getSlim()->stop();
        }

        if(!empty($user['id_customer'])){
            $answer['kind_of_user'] = 'customer';
            $answer['customer']     = $customerService->getCustomer($user['id_customer']);
        }

        if(!empty($user['id_supplier'])){
            $answer['kind_of_user'] = 'supplier';
            $answer['supplier']     = $supplierService->getSupplier($user['id_supplier']);
        }

        if(!empty($user['id_admin'])){
            $answer['kind_of_user'] = 'admin';
            $answer['admin']        = $adminService->getAdministrator($user['id_admin']);
        }

        return $answer;
    }

    /**
     * Validade create params
     */
    public function checkParamsCreate($obj_params){

        if (
                empty($obj_params['email'])
            ||  empty($obj_params['password'])
        ){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   =>  "BadRequest",
                'message'  => "Missing fields",
                'required' => array('email', 'password')
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Validade update params
     */
    public function checkParamsUpdate($obj_params){

        if (empty($obj_params['token'])) {
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Missing fields",
                'required' => array('token')
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Gera nova senha para usuário
     */
    public function forgotPassword(){

        $alert       = array();
        $userService = new UserService();
        $obj_params  = $this->getBodyRequest();
        
        if(empty($obj_params['email'])){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status' =>  "BadRequest",
                'message' => "Missing fields. "
                )
            );
            $this->getSlim()->stop();
        }

        $user = $userService->getUserByEmail($obj_params['email']);
        if(!$user){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status' =>  "InvalidEmail",
                'message' => "Invalid email. "
                )
            );
            $this->getSlim()->stop();
        }

        $obj_params['new_password'] = $userService->forgotPassword($obj_params['email']);
        if(!$obj_params['new_password']){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status' =>  "InternalServerError",
                'message' => "Internal server error on create new password for the user. "
                )
            );
            $this->getSlim()->stop();
        }

        $mail = $this->sendEmailForgotPassword($obj_params);
        if(!empty($mail['alert'])){

            $alert = array_merge($alert, $mail['alert']);
        }

        self::response(self::STATUS_NO_CONTENT, array('alert' => $alert));
    }

    /**
     * Get all info necessary and send email of forgot password
     */
    public function sendEmailForgotPassword($params){

        $email       = new MyEmail();
        $userService = new UserService();

        $alert  = array();
        $status = true;

        $user = $userService->getUserByEmail($params['email']);
        $doc  = new DOMDocument();
        $doc->loadHTMLFile(__DIR__.'/../../../config/email/forgot_password.html');
        
        $doc->getElementById('new_password')->nodeValue = $params['new_password'];
        $doc->getElementById('user_name')->nodeValue    = $user['name'];

        $mail = $email->sendEmail(
             $params['email']
            ,$doc->saveHTML()
            ,'Drinkeat - Esqueci minha senha'
            ,false
        );

        if(!$mail['status']){
            $status = false;
            $alert[] = $mail['alert'];
        }

        return array(
            'status' => $status,
            'alert'  => $alert
        );
    }

    /**
     * Show options in header
     */
    public function options(){

        self::response(self::STATUS_OK, array(), array('POST', 'OPTIONS'));
    }

    /**
     * @return \App\Service\Authorize
     */
    public function getAuthorizeService(){

        return $this->authorizeService;
    }

    /**
     * @param \App\Service\AUthorize $authorizeService
     */
    public function setAuthorizeService($authorizeService){

        $this->authorizeService = $authorizeService;
    }

    /**
     * @return array
     */
    public function getOptions(){

        return $this->options;
    }
}