<?php

namespace App\Resource;

use App\Resource;
use App\Service\Project as ProjectService;
use App\Resource\Project as ProjectResource;

class Project extends Resource
{
    /**
     * @var \App\Service\Project
     */
    private $projectService;
    private $log_file = "project_resource.log";

    /**
     * Get project service
     */
    public function init(){

        $this->setProjectService(new ProjectService());
    }

    /**
     * Register project
     */
    public function registerProject($id_mpe, $obj_params){

    	$this->checkParamsCreate($obj_params);

        $id_project = $this->getProjectService()->registerProject($id_mpe, $obj_params);
        if(!$id_project){
            self::response(self::STATUS_INTERNAL_SERVER_ERROR, array(
                'status'   => "InternalServerError",
                'message'  => "Internal server error on register project."
                )
            );
            $this->getSlim()->stop();
        }

        return $id_project;
    }

    /**
     * Validade create params
     */
    public function checkParamsCreate($obj_params){

        if (
               empty($obj_params['month_consumption'])
            || empty($obj_params['financing_time_month'])
            || empty($obj_params['financing_tax'])
            || empty($obj_params['plot_value'])
            || empty($obj_params['project_value'])
            || empty($obj_params['initial_payment'])
        ){
            self::response(self::STATUS_BAD_REQUEST, array(
                'status'   => "BadRequest",
                'message'  => "Missing fields",
                'required' => array(
                     'month_consumption'
                    ,'financing_time_month'
                    ,'financing_tax'
                    ,'plot_value'
                    ,'project_value'
                    ,'initial_payment'
                )
                )
            );
            $this->getSlim()->stop();
        }
    }

    /**
     * Show options in header
     */
    public function options(){

        self::response(self::STATUS_OK, array(), array('POST', 'OPTIONS'));
    }

    /**
     * @return \App\Service\Project
     */
    public function getProjectService(){

        return $this->projectService;
    }

    /**
     * @param \App\Service\AUthorize $projectService
     */
    public function setProjectService($projectService){

        $this->projectService = $projectService;
    }

    /**
     * @return array
     */
    public function getOptions(){

        return $this->options;
    }
}