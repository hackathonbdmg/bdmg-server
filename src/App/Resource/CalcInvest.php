<?php

namespace App\Resource;

use App\Resource;

class CalcInvest extends Resource
{
    /**
     * @var \App\Service\Contact
     */
    private $log_file = "calc_invest.log";

    /**
     * Get contact service
     */
    public function init(){

    }

    /**
     * Get calc invest
     */
    public function getCalcInvest(){

        $env    = $this->getSlim()->environment('user');
        $req    = $this->getSlim()->request();
        $params = array(
            'id_state' => $req->get('id-state'),
            'id_calc'  => $req->get('id-calc'),
            'kwp'      => $req->get('kwp')
        );

        $ch = curl_init();

		$url = "https://www.portalsolar.com.br/Simulador/CalcularSimulador?";

		$url .= "EstadoId=".$params['id_state'];
		$url .= "&CalculadoraId=".$params['id_calc'];
		$url .= "&KWP=".$params['kwp'];


        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array()));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json"
        ));

        $response  = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        switch ($http_code) {
            case 200:
                $response = $this->getCalcResponse($response);
                break;

            default:
            	$response = array(
		    		 'size'            => "7,95"
		    		,'year_production' => "12000"
		    		,'minimum_area'    => "63,63"
		    		,'average_pound'   => "15"
		    		,'plate_number'    => "31"
		    		,'max_cost'        => "R$ 46.110,00"
		    		,'min_cost'        => "R$ 38.160,00"
		    		,'average'         => "1000"
		    	);
                break;
        }

        self::response(self::STATUS_OK, $response);
    }

    public function getCalcResponse($response){

    	$response = json_decode($response);
    	
    	return array(
    		 'size'            => $response->{'tamanho'}
    		,'year_production' => $response->{'producaoAnual'}
    		,'minimum_area'    => $response->{'areaMinima'}
    		,'average_pound'   => $response->{'pesoMedio'}
    		,'plate_number'    => $response->{'qtdPLacas'}
    		,'max_cost'        => $response->{'custoMax'}
    		,'min_cost'        => $response->{'custoMin'}
    		,'average'         => $response->{'media'}
    	);
    }

    /**
     * Show options in header
     */
    public function options(){

        self::response(self::STATUS_OK, array(), array('POST', 'OPTIONS'));
    }

    /**
     * @return array
     */
    public function getOptions(){

        return $this->options;
    }
}