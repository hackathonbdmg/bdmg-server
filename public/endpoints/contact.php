<?php   

    $app->get('/contact/', $autenticationAdmin, function() use($app){
        $resource = \App\Resource::load('contact');
        $resource->getContacts();
    });

    $app->post('/contact', $verifyUser, function() use($app){
        $resource = \App\Resource::load('contact');
        $resource->registerContact();
    });

?>