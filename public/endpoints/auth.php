<?php   

    $app->post('/auth/authorize', function() use($app){
        $resource = \App\Resource::load('authorize');
        $resource->post();
    });

    $app->put('/auth/forgot-password', function() use($app){
        $resource = \App\Resource::load('authorize');
        $resource->forgotPassword();
    });
?>