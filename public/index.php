<?php

require_once __DIR__ . '/../class-loader.php';

$logWriter = new \Slim\LogWriter(fopen('../logs/SLIM_errors.log', 'a'));
$app = new Slim\Slim(array('log.writer' => $logWriter));
$app->config('debug', true); 

$app->add(new \CorsSlim\CorsSlim());

$app->map('/:x+', function($x) {
    http_response_code(200);
})->via('OPTIONS');

//Get default
$app->get('/', function() use($app){
    $app->response->headers->set('Content-Type', 'text/html');
    echo "API BDMG V 1.0.0";
    die();
});


//MiddleWare de validação de usuario
$autenticationUser = function(){

    $app = Slim\Slim::getInstance();

    $token = $app->request->headers->get('Authorization');

    $library = \App\Library::load('authorize');

    $library->verifyTokenUser($token);
};

//MiddleWare de validação de customer
$autenticationPme = function(){

    $app = Slim\Slim::getInstance();

    $token = $app->request->headers->get('Authorization');

    $library = \App\Library::load('authorize');

    $library->verifyTokenCustomer($token);
};

//MiddleWare de validação de supplier
$autenticationIntegrador = function(){

    $app = Slim\Slim::getInstance();

    $token = $app->request->headers->get('Authorization');

    $library = \App\Library::load('authorize');

    $library->verifyTokenSupplier($token);
};

//MiddleWare de verificação de usuário
$verifyUser = function(){

    $app = Slim\Slim::getInstance();

    $token = $app->request->headers->get('Authorization');

    $library = \App\Library::load('authorize');

    $library->verifyTokenUser($token, false);
};


//Routers API
$app->group('/api', function() use($app, $autenticationUser, $autenticationPme, $autenticationIntegrador, $verifyUser){
    $app->response->headers->set('Content-Type', 'application/json');

    require_once __DIR__ . '/endpoints/auth.php';

    require_once __DIR__ . '/endpoints/calc.php';

    require_once __DIR__ . '/endpoints/mpe.php';
});

// Not found
$app->notFound(function() {
    \App\Resource::response(\App\Resource::STATUS_NOT_FOUND);
});

$app->run();

